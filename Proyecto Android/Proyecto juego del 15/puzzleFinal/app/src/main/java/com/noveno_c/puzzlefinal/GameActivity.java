package com.noveno_c.puzzlefinal;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity {

    private int emptyX =3;
    private int emptyY=3;
    private RelativeLayout group;
    private Button [] [] buttons;
    private int [] tiles;
    private TextView textViewSteps;
    private int stepCount = 0;
    private TextView textViewTime;
    private Timer timer;
    private int timeCount=0;
    private Button botonRevolver,trampa,boton1,boton2;
    private Button buttonStop;
    private boolean isTimeRnning;
    private Button detalles;
    TextView saludo,aviso;
    private int second;
    private int hour;
    private int bandera = 0;
    private int minute;
    private String horaFinal;
    private String valor;
    private  String [][] matriz = new String [4][4];
    String vector [] = new String [16] ;
    String vector2 [] = new String [16] ;
    String vector3 []= new String [16];
    String vectorAux[]=new String [12];
    private String forma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        cargarViews();
        cargarNumeros();
        generarNumerosRandom();
        cargarDataViews();
        matrizPerfiferico();
        matrizEspiral();
        matrizImposible();
        saludo = findViewById(R.id.saludo);
        aviso= findViewById(R.id.aviso);
        trampa = findViewById(R.id.botonTrampa);
        trampa.setVisibility(View.INVISIBLE);

        detalles = findViewById(R.id.detalles);
        detalles.setVisibility(View.INVISIBLE);
        boton1=findViewById(R.id.button1);
        boton2=findViewById(R.id.button2);


        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        valor = preferences.getString("nombre","No existe");
        //valor = getIntent().getExtras().getString("name");

        saludo.setText("¡Hola, "+valor+"!");


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.objetivo:
                new AlertDialog.Builder(this)
                        .setTitle("Objetivo")
                        .setMessage(
                                "El objetivo del juego es colocar las piezas en orden del 1-15 ya sea de forma (vertical, periférico, espiral, imposible o tradicional). Siendo que al momento de tocar un número, éste se desplazará automáticamente al espacio que está vacío.\n")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                }).show();

                return true;
            case R.id.comoJugar:

                Intent intent = new Intent(this,comoJugar.class);
                startActivity(intent);
                //  showHelp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void cargarDataViews() {
        emptyX=3;
        emptyY=3;
        for (int i = 0; i<group.getChildCount()-1;i++){
            buttons[i/4][i%4].setText(String.valueOf(tiles[i]));
            buttons[i/4][i%4].setBackgroundResource(android.R.drawable.btn_default);

        }
        buttons[emptyX][emptyY].setText("");
        buttons[emptyX][emptyY].setBackgroundColor(ContextCompat.getColor(this,R.color.colorFreeButton));

    }

    private void generarNumerosRandom() {
        int n=15;
        Random randon = new Random ();
        while (n>1){
            int randomNum = randon.nextInt(n--);
            int temp = tiles [randomNum];
            tiles [randomNum]=tiles[n];
            tiles[n]=temp;
        }
        if(!tieneSolucion()){
            generarNumerosRandom();
        }
        
    }

    private boolean tieneSolucion() {
        int countInversions=0;
        for (int i=0;i<15;i++){
            for (int j=0;j<i;j++){
                if(tiles[j]>tiles[i])
                    countInversions++;
            }
        }
        return  countInversions%2 ==0;
    }


    private void cargarNumeros() {
        tiles = new int[16];
        for (int i=0;i<group.getChildCount()-1;i++){
            tiles[i]= i +1;
        }
    }

    private void cargarTiempo(){
        isTimeRnning= true;
        timer = new Timer ();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timeCount++;
                setTime(timeCount);
            }
        },1000,1000);
    }

    private void setTime (int timeCount){
        second= timeCount % 60;
        hour = timeCount / 3600;
        minute= (timeCount-hour*3600) / 60;
        textViewTime.setText(String.format("Tiempo: %02d:%02d:%02d",hour,minute,second ));
    }
    private void cargarViews() {
        group = findViewById(R.id.group);
        textViewSteps = findViewById(R.id.text_view_setps);
        textViewTime=findViewById(R.id.text_view_time);
        botonRevolver = findViewById(R.id.button_shuffle);
        buttonStop = findViewById(R.id.button_stop);

        cargarTiempo();
        buttons = new Button[4][4];
        for (int i = 0; i < group.getChildCount();i++){
            buttons[i/4][i%4] = (Button) group.getChildAt(i);
        }

        botonRevolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stepCount=0;
                timeCount=0;
                trampa.setVisibility(View.INVISIBLE);
                aviso.setText("");
                textViewTime.setText("Tiempo: 00.00.00");
                textViewSteps.setText("Movimientos: "+ 0);
                generarNumerosRandom();
                cargarDataViews();
            }
        });
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTimeRnning){
                    timer.cancel();
                    buttonStop.setText("Reaunudar");
                    isTimeRnning=false;
                    for (int i=0;i <group.getChildCount();i++){
                        buttons[i/4][i%4].setClickable(false);
                    }
                }
                else{
                    cargarTiempo();
                    buttonStop.setText("Detener");
                    for (int i=0;i <group.getChildCount();i++){
                        buttons[i/4][i%4].setClickable(true);
                    }
                }
            }
        });
    }

    //Inicia métodos para la matriz espiral
    private static Object spiral(int x, int y) {

        int c = max(abs(x), abs(y)); // el círculo donde se encuentran las coordenadas actuales

        int max = (c * 2 + 1) * (c * 2 + 1); // el valor máximo en el círculo actual

        if (y == -c) {// arriba

            return max + (x + y);

        } else if (x == -c) {// left

            return max + (3 * x - y);

        } else if (y == c) {// abajo

            return max + (-x - 5 * y);

        }
        {// derecha

            return max + (-7 * x + y);

        }

    }

    private static int max(int n1, int n2) {

        return n1 > n2 ? n1 : n2;

    }

    private static int abs(int x) {

        return x > 0 ? x : -x;

    }
    //finaliza metodos para la matriz espiral


    //inicia llenado de la matriz espiral
    private void matrizEspiral (){

        int cont=0;

        for (int y = -1; y <= 2; ++y) {

            for (int x = -1; x <= 2; ++x) {
                vector2 [cont] = String.valueOf(spiral(x, y));
                cont++;
            }


        }

        vector2[12]="";

        for (int i = 0; i<vector2.length;i++){
          //  System.out.print(vector2[i]+ " ");
        }
    }
    //finaliza llenado matriz espiral

    private void matrizPerfiferico (){

        int cont=0;
        int a = 0;
        int b= 3;
        int valor = 1,aux=0;

        for (int j=0; j<matriz.length;j++){
            for (int i= a; i<=b; i++){
                matriz[a][i]=String.valueOf(valor++);
            }

            for (int i = a + 1; i<= b;i++){
                matriz[i][b]= String.valueOf(valor++);
            }

            for (int i =b-1; i>=a;i--){
                matriz[b][i]= String.valueOf(valor++);
            }

            for (int i = b-1; i>=a+1;i--){

                matriz[i][a]= String.valueOf(valor++);


            }
            matriz[2][1]="";
            a++; b--;

        }//fin ciclo externo

        for (int f = 0; f<matriz.length;f++){
            for (int i= 0; i<matriz[f].length;i++){

                vector[cont] = matriz[f][i];
                cont++;
            }

        }

       // for (int j = 0; j<vector.length;j++){
         //   System.out.println(vector[j]);
        //}
    }

    private void matrizImposible (){
        int cont = 15;

        for (int i = 0; i <16 ; i++){
            if (i<=11){
                vectorAux[i]= String.valueOf(cont);
            }
            vector3[i]= String.valueOf(cont--);

        }
        vector3[15] = "";

        for (int i = 0; i <16 ; i++){
         //   System.out.println( vector3[i]);
        }

    }

    public void buttonClick(View view){
        Button button = (Button) view;
        int x = button.getTag().toString().charAt(0)- '0';
        int y = button.getTag().toString().charAt(1)- '0';

        if ((Math.abs(emptyX-x)==1&&emptyY==y) || (Math.abs(emptyY-y)==1&&emptyX==x)){
            buttons[emptyX][emptyY].setText(button.getText().toString());
            buttons[emptyX][emptyY].setBackgroundResource(android.R.drawable.btn_default);
            button.setText("");
            button.setBackgroundColor(ContextCompat.getColor(this,R.color.colorFreeButton));
            emptyX=x;
            emptyY=y;
            stepCount++;
            textViewSteps.setText("Movimientos: "+ stepCount);


            verificarSiGano();
        }
    }

    private void verificarSiGano() {
        int aux = 0;
        int cont = 1;
        int correctoVertical=0;

        boolean isWin = false;

        if(emptyX==3&&emptyY==3){
            for (int i = 0; i<group.getChildCount()-1 ; i++){
                if (buttons[i/4][i%4].getText().toString().equals(String.valueOf(i+1))){
                    forma = "Tradicional";
                    isWin=true;
                }
                else if (buttons[i%4][i/4].getText().toString().equals(String.valueOf(i+1))){
                    forma = "Vertical";
                    isWin=true;
                }
                else if (buttons[i / 4][i % 4].getText().toString().replaceAll(" ","").equals(String.valueOf(vector3[i]))){
                    forma = "Imposible";
                    isWin=true;
                }
                else{
                    isWin=false;
                 //   trampa.setVisibility(View.INVISIBLE);
                  break;
                }
            }
            for (int i = 0; i<12 ; i++){
                    if (buttons[i / 4][i % 4].getText().toString().equals(String.valueOf(vectorAux[i]))) {
                        trampa.setVisibility(View.VISIBLE);
                    }
                    else{
                        if (bandera==0) {
                            trampa.setVisibility(View.INVISIBLE);
                        }
                        break;
                    }
            }
        } else if (emptyX==2 && emptyY==1){
            for (int i = 0; i<group.getChildCount()-1 ; i++) {

                if (buttons[i / 4][i % 4].getText().toString().equals(String.valueOf(vector[i]))) {
                    forma = "Periférico";
                    isWin = true;
                }
                else{
                    isWin=false;
                 //   trampa.setVisibility(View.INVISIBLE);
                    break;
                }
            }
        }else if (emptyX==3 && emptyY==0){
            for (int i = 0; i<group.getChildCount()-1 ; i++) {

                if (buttons[i / 4][i % 4].getText().toString().equals(String.valueOf(vector2[i]))) {
                    forma = "Espiral";
                    isWin = true;
                }
                else{
                    isWin=false;
              //      trampa.setVisibility(View.INVISIBLE);
                    break;
                }
            }
        }
        if (isWin){
            Toast.makeText(this,"Ganaste\nMovimientos: "+stepCount,Toast.LENGTH_SHORT).show();
            for (int i = 0 ; i < group.getChildCount(); i++){
                buttons[i/4][i%4].setClickable(false);
            }

              timer.cancel();
           // botonRevolver.setClickable(false);
           // buttonStop.setClickable(false);
              botonRevolver.setVisibility(View.INVISIBLE);
              buttonStop.setVisibility(View.INVISIBLE);
              detalles.setVisibility(View.VISIBLE);
              horaFinal= String.format("%02d:%02d:%02d",hour,minute,second );


        }

    }


    public void abrirDetalles(View view){
        //String aux = texto.getText().toString();

            Intent intent = new Intent(this,resumenActivity.class);

            intent.putExtra("tiempo",horaFinal);
            intent.putExtra("numero",String.valueOf(stepCount));
           // intent.putExtra("name",valor);
            intent.putExtra("forma",forma);

            startActivity(intent);

    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void activarTrampa (View view){
       bandera = 1;

        for (int i = 0; i<group.getChildCount()-1 ; i++){

            if (buttons[i / 4][i % 4].getText().toString().equals("1")) {
                buttons[i / 4][i % 4].setText("2 ");

            }
            if (buttons[i / 4][i % 4].getText().toString().equals("2")) {
                buttons[i / 4][i % 4].setText("1");
            }



        }
        trampa.setText("¡TRAMPA ACTIVADA!");
        trampa.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#028100")));
        aviso.setText("Ya no podrás solucionar de otra manera, puedes iniciar una nueva partida");
        trampa.setClickable(false);
        verificarSiGano();

        //boton1.setText("2");
        //boton2.setText("1");
    }

}