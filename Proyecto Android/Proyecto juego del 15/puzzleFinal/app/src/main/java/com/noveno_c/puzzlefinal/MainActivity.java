package com.noveno_c.puzzlefinal;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button buttonStarGame;
    EditText texto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        texto= findViewById(R.id.editNombre);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.objetivo:
                new AlertDialog.Builder(this)
                        .setTitle("Objetivo")
                        .setMessage(
                                "El objetivo del juego es colocar las piezas en orden del 1-15 ya sea de forma (vertical, periférico, espiral, imposible o tradicional). Siendo que al momento de tocar un número, éste se desplazará automáticamente al espacio que está vacío.\n")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                }).show();

                return true;
            case R.id.comoJugar:

                Intent intent = new Intent(this,comoJugar.class);
                startActivity(intent);
              //  showHelp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void abrirPuzzle(View view){
        //String aux = texto.getText().toString();

        if(texto.getText().toString().isEmpty()){
            Toast.makeText(this,"¡Debe ingresar un nombre!",Toast.LENGTH_SHORT).show();
        }
        else{
            Intent intent = new Intent(this,GameActivity.class);
            SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("nombre",texto.getText().toString());
            editor.commit();

            intent.putExtra("name",texto.getText().toString());

            startActivity(intent);
        }
    }
}