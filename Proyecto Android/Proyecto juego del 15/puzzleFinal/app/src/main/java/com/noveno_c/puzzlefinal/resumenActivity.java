package com.noveno_c.puzzlefinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class resumenActivity extends AppCompatActivity {
    TextView texto1, texto2, texto3,texto4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen);
        texto1 = findViewById(R.id.nombre);
        texto2 = findViewById(R.id.tiempo);
        texto3 = findViewById(R.id.numero);
        texto4 = findViewById(R.id.forma);

        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);


        String valor = preferences.getString("nombre","No existe");
        String valor2 = getIntent().getExtras().getString("tiempo");
        String valor3 = getIntent().getExtras().getString("numero");
        String valor4 = getIntent().getExtras().getString("forma");

        texto1.setText(valor);
        texto2.setText(valor2);
        texto3.setText(valor3);
        texto4.setText(valor4);

    }


    public void abrirPuzzle(View view){

            Intent intent = new Intent(this,GameActivity.class);
            startActivity(intent);

    }
    public void cerrarSesion(View view){

        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);

    }

    public void salir(View view){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}