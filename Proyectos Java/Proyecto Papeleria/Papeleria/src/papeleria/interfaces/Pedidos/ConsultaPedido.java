/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces.Pedidos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import papeleria.interfaces.PantallaInicio;


public class ConsultaPedido extends javax.swing.JFrame {

    /**
     * Creates new form ConsultaPedido
     */
    
    Connection con = null;
    Statement stmt = null;
    
   public static String idPedid = "";
    
    String titulos[] = {"ID", "Fecha", "Proveedor", "Empleado"};
    String fila[] = new String[4];
    DefaultTableModel modelo;
    
    public ConsultaPedido() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        Consulta();
    }

    public void Consulta(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            double totalSist = 0;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Pedido");
            String Empleado = "";
            String Proveedor = "";

            modelo = new DefaultTableModel(null, titulos);

            while (rs.next()) {

                fila[0] = rs.getString("IdPedido");
                fila[1] = rs.getString("Fecha");
                Proveedor = obtenerNombre(rs.getString("IdProveedor"),0);
                fila[2] = Proveedor;
                Empleado = obtenerNombre(rs.getString("IdEmpleado"),1);
                fila[3] = Empleado;                

                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            TableColumn id = Consultas.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn fec = Consultas.getColumn("Fecha");
            fec.setMaxWidth(300);
            TableColumn prov = Consultas.getColumn("Proveedor");
            prov.setMaxWidth(300);          
            TableColumn emp = Consultas.getColumn("Empleado");
            emp.setMaxWidth(300);

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
    
    public String obtenerNombre (String id, int EmpPro){
        String Nombre = "";
        try{
            stmt = con.createStatement();
            
            if(EmpPro == 1){
                ResultSet rs = stmt.executeQuery("select * from Empleado WHERE IdEmpleado = " + id);

                while (rs.next()) {
                    Nombre = rs.getString("IdEmpleado") + " - " + rs.getString("Nombre");
                }                
            }
            else
            {
                ResultSet rs = stmt.executeQuery("select * from Proveedor WHERE IdProveedor = " + id);

                while (rs.next()) {
                    Nombre = rs.getString("IdProveedor") + " - " + rs.getString("NombreComercial");
                }                  
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaPedido.class.getName()).log(Level.SEVERE, null, ex);
        }
 
        return Nombre;
    }    
    
    public void Elimina(){  
        try {
            int filaSeleccionada = Consultas.getSelectedRow();
            String SQLelim = "DELETE FROM Pedido WHERE idPedido = " + Consultas.getValueAt(filaSeleccionada, 0);
            System.out.println(SQLelim);
            Statement st= con.createStatement();
            
            int n = st.executeUpdate(SQLelim);
            
            if(n >= 0){
                javax.swing.JOptionPane.showMessageDialog(this, "Pedido Eliminado Correctamente", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            } 
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(this, "Error al eliminar registro", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            Logger.getLogger(ConsultaPedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        catch(IndexOutOfBoundsException e){
            javax.swing.JOptionPane.showMessageDialog(this, "Selecciona un elemento de la Tabla.", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            Logger.getLogger(ConsultaPedido.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ConsultaPedido = new javax.swing.JButton();
        nuevoProducto = new javax.swing.JButton();
        Regresar2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Consultas = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        Modificar = new javax.swing.JButton();
        Eliminar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(102, 153, 255));

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("PEDIDOS");

        ConsultaPedido.setBackground(new java.awt.Color(153, 0, 0));
        ConsultaPedido.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        ConsultaPedido.setForeground(new java.awt.Color(255, 255, 255));
        ConsultaPedido.setText("Consultar");
        ConsultaPedido.setBorder(null);
        ConsultaPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaPedidoActionPerformed(evt);
            }
        });

        nuevoProducto.setBackground(new java.awt.Color(153, 0, 0));
        nuevoProducto.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        nuevoProducto.setForeground(new java.awt.Color(255, 255, 255));
        nuevoProducto.setText("Nuevo Pedido");
        nuevoProducto.setBorder(null);
        nuevoProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoProductoActionPerformed(evt);
            }
        });

        Regresar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Regresar.png"))); // NOI18N
        Regresar2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Regresar2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(Regresar2)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addComponent(nuevoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 206, Short.MAX_VALUE)
                        .addComponent(ConsultaPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(139, 139, 139))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Regresar2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ConsultaPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nuevoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel1.setBackground(new java.awt.Color(0, 153, 102));

        Consultas.setBackground(new java.awt.Color(102, 255, 102));
        Consultas.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        Consultas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Id", "Fecha", "Duenio"
            }
        ));
        Consultas.setFocusable(false);
        Consultas.setGridColor(new java.awt.Color(255, 102, 51));
        Consultas.setSelectionBackground(new java.awt.Color(225, 225, 225));
        Consultas.setSelectionForeground(new java.awt.Color(0, 0, 0));
        jScrollPane1.setViewportView(Consultas);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Consulta");

        Modificar.setBackground(new java.awt.Color(255, 255, 255));
        Modificar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Modificar.setText("M O D I F I C A R");
        Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarActionPerformed(evt);
            }
        });

        Eliminar.setBackground(new java.awt.Color(255, 255, 255));
        Eliminar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Eliminar.setText("E L I M I N A R");
        Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 911, Short.MAX_VALUE)
                .addGap(48, 48, 48))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Modificar)
                .addGap(226, 226, 226)
                .addComponent(Eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(189, 189, 189))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(44, 44, 44)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(45, 45, 45)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Modificar)
                    .addComponent(Eliminar))
                .addContainerGap(22, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel6)
                    .addContainerGap(548, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ConsultaPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaPedidoActionPerformed
        // TODO add your handling code here:
        ConsultaPedido consulta = new ConsultaPedido();
        consulta.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ConsultaPedidoActionPerformed

    private void nuevoProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevoProductoActionPerformed
        RealizarPedido inserta = new RealizarPedido();
        inserta.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_nuevoProductoActionPerformed

    private void Regresar2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Regresar2MouseClicked
        PantallaInicio abrir = new PantallaInicio();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_Regresar2MouseClicked

    private void ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarActionPerformed
        int filaSeleccionada = Consultas.getSelectedRow();
        idPedid = (String) Consultas.getValueAt(filaSeleccionada, 0);

        ModificaPedido modificar = new ModificaPedido();
        modificar.setVisible(true);
        this.setVisible(false);
        //System.out.println(idProduct);
    }//GEN-LAST:event_ModificarActionPerformed

    private void EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarActionPerformed
        //Panel de Advertencia
        int seleccion = JOptionPane.showOptionDialog(this,"¿Esta seguro de que eliminar el Pedido?",
            "ADVERTENCIA",JOptionPane.WARNING_MESSAGE,
            JOptionPane.WARNING_MESSAGE,null,// null para icono por defecto.
            new Object[] { "Aceptar", "Cancelar"},"Aceptar");
        //int hacerCorte = JOptionPane.showConfirmDialog(null, "¿Esta seguro de que desea realizar el corte? \n (Dicha opción no se puede deshacer).", "ADVERTENCIA", WIDTH, HEIGHT);

        if(seleccion == 0){
            Elimina();
            Consulta();
        }

    }//GEN-LAST:event_EliminarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultaPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new ConsultaPedido().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ConsultaPedido;
    private javax.swing.JTable Consultas;
    private javax.swing.JButton Eliminar;
    private javax.swing.JButton Modificar;
    private javax.swing.JLabel Regresar2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton nuevoProducto;
    // End of variables declaration//GEN-END:variables
}
