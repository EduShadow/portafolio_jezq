-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-09-2021 a las 23:07:34
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cq`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(5) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apaterno` varchar(45) NOT NULL,
  `Amaterno` varchar(45) NOT NULL,
  `FN` date NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Sexo` enum('M','F') NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `Nombre`, `Apaterno`, `Amaterno`, `FN`, `Telefono`, `Estado`, `Sexo`, `Correo`, `Password`) VALUES
(1, 'German', 'Garmendia', 'Perez', '2020-05-14', '245895623', 'Tlaxcala', 'M', 'correo@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat_mensaje`
--

CREATE TABLE `chat_mensaje` (
  `id_chat_mensaje` int(10) UNSIGNED NOT NULL,
  `m_chat_usuario` varchar(15) NOT NULL,
  `chat_mensaje` varchar(140) NOT NULL,
  `creado_en` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `idCita` int(5) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `Sintomas` varchar(50) NOT NULL,
  `MetodoPago` enum('Efectivo','Tarjeta de Crédito/Débito') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`idCita`, `Fecha`, `Hora`, `Direccion`, `Sintomas`, `MetodoPago`) VALUES
(1, '2020-12-09', '14:00:00', 'Calle del bienestar 6', 'Me duele la panza', 'Efectivo'),
(2, '2020-12-09', '12:00:00', 'Calle flores s/n', 'Dolor de cabeza\r\nTemperatura', 'Efectivo'),
(3, '2020-12-09', '12:22:00', 'Calle glomez', 'Lele panza', 'Efectivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctor`
--

CREATE TABLE `doctor` (
  `idDoctor` int(5) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apaterno` varchar(40) NOT NULL,
  `Amaterno` varchar(40) NOT NULL,
  `FN` date NOT NULL,
  `Cedula` int(9) NOT NULL,
  `Especialidad` varchar(40) NOT NULL,
  `CostoConsulta` double(5,2) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Sexo` enum('M','F') NOT NULL,
  `Experiencia` text NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `doctor`
--

INSERT INTO `doctor` (`idDoctor`, `Nombre`, `Apaterno`, `Amaterno`, `FN`, `Cedula`, `Especialidad`, `CostoConsulta`, `Telefono`, `Estado`, `Sexo`, `Experiencia`, `Correo`, `Password`) VALUES
(4, 'Sandy', 'Carranza', 'Mendez', '1972-12-06', 2774728, 'Psicología', 99.99, '2467282812', 'Estado de México', 'M', 'Egresado de Harvard', 'sandy123@gmail.com', '1234'),
(5, 'Roman', 'Perez', 'Carranza', '1992-02-01', 89123874, 'Psicología', 99.00, '8742378', 'Estado de México', 'M', 'Egrese de la UPTX', 'eldr@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hojaexpediente`
--

CREATE TABLE `hojaexpediente` (
  `idHojaExpediente` int(5) NOT NULL,
  `Fecha` date NOT NULL,
  `Peso` float(3,2) NOT NULL,
  `Altura` float(3,2) NOT NULL,
  `Temperatura` float(3,2) NOT NULL,
  `Motivo` text NOT NULL,
  `Enfermedad` varchar(50) NOT NULL,
  `Antecedentes` varchar(50) NOT NULL,
  `Vicios` text NOT NULL,
  `EstudiosRealizados` varchar(60) NOT NULL,
  `Diagnostico` text NOT NULL,
  `MedicamentosRecetados` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `idPaciente` int(5) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apaterno` varchar(40) NOT NULL,
  `Amaterno` varchar(40) NOT NULL,
  `FN` date NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Sexo` enum('M','F') NOT NULL,
  `Correo` varchar(40) NOT NULL,
  `Password` varchar(40) NOT NULL,
  `tipoAprendizaje` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`idPaciente`, `Nombre`, `Apaterno`, `Amaterno`, `FN`, `Telefono`, `Estado`, `Sexo`, `Correo`, `Password`, `tipoAprendizaje`) VALUES
(25, 'Juan', 'Pérez', 'Ortega', '2000-06-19', '2461523698', 'TLX', 'M', 'casjnnjd@gmail.com', '1234', ''),
(26, 'Francisco', 'Jimenez', 'Vigueras', '1998-04-27', '2415268596', 'YUC', 'M', 'correoluis@gmail.com', '1234', 'Visual');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `chat_mensaje`
--
ALTER TABLE `chat_mensaje`
  ADD PRIMARY KEY (`id_chat_mensaje`);

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`idCita`);

--
-- Indices de la tabla `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`idDoctor`);

--
-- Indices de la tabla `hojaexpediente`
--
ALTER TABLE `hojaexpediente`
  ADD PRIMARY KEY (`idHojaExpediente`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`idPaciente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `chat_mensaje`
--
ALTER TABLE `chat_mensaje`
  MODIFY `id_chat_mensaje` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `idCita` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `doctor`
--
ALTER TABLE `doctor`
  MODIFY `idDoctor` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `hojaexpediente`
--
ALTER TABLE `hojaexpediente`
  MODIFY `idHojaExpediente` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `idPaciente` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
