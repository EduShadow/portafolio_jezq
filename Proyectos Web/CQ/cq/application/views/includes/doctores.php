<!-- testemonial Start -->
<section id="res"   class="testemonial">
			<div class="container">

				<div class="gallary-header text-center">
					<h2>
						Estilos de aprendizaje
					</h2>
					<p>
					El aprendizaje es una construcción humana, que implica una serie de procesos, métodos y estrategias. 
					Cada alumno es capaz de implementar su propio método o estrategia para construir su aprendizaje, es decir, 
					cada alumno tiene su propio estilo de aprendizaje.
					</p>

				</div><!--/.gallery-header-->

				<div class="owl-carousel owl-theme" id="testemonial-carousel">

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src= "https://i.ibb.co/8Kcw17G/visual.png" alt="img"/>
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							      
									* Aprenden mejor cuando leen y escriben información.<br>
									* Necesitan explicaciones acompañadas de imágenes o gráficas.<br>
									* Necesitan observar y ser observados, es decir, tener contacto visual con el objeto de aprendizaje.<br>
									* Tienen la facilidad de absorber mucha información en menos tiempo.<br>
									* Desarrollan la capacidad de abstracción y planificación.<br>
									* Son organizados.<br>
									* Hacen muchas cosas a la vez.<br>
	
							</p>
								<h3>
									<a href="#">
										Visual
									</a>
								</h3>
								<h4> Representación mental que a través del sentido de la vista y recursos visuales, se favorece la comprensión de objetos de aprendizaje.</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src=  "https://i.ibb.co/2P6C1t3/auditivo.png" alt="img"/>
							
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							 
									* Aprenden mejor con explicaciones orales.<br>
									* Aprenden a través de la memorización.<br>
									* Necesitan que las imágenes o carteles, vayan acompañados de consignas verbales.<br>
									* Solo pueden hacer una cosa a la vez.<br>
									* Escuchan atentamente y necesitan ser escuchados, para recibir una retroalimentación.<br>
									* Mueven los labios al leer.<br>
									* Tienen facilidad de palabra.<br><br><br>

							</p>
								<h3>
									<a href="#">
									Auditivos 
									</a>
								</h3>
								<h4> El sistema de representación auditiva se refiere a la asimilación de la información a través del oído y no por la vista.</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src=      "https://i.ibb.co/cy7LGKr/kinestesicooooo.png" alt="img"/>
							
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							     
								* Requieren situaciones de aprendizaje dinámico.<br>
								* Aprenden lo que experimentan directamente.<br>
								* Aprenden con lo que tocan y lo que hacen.<br>
								* Su proceso de aprendizaje es lento, pero profundo.<br>
								* Necesitan estar involucrados directamente con alguna actividad.<br>
								* Les cuesta trabajo comprender lo que no pueden poner en práctica.<br><br><br>

							</p>
								<h3>
									<a href="#">
									Kinestésico
									</a>
								</h3>
								<h4>Cuando se procesa información asociándola con sensaciones y movimientos, 
								se hace referencia al sistema de representación kinestésico, el cual, potencializa el aprendizaje de un deporte, manualidades, entre otros.</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

				

				</div><!--/.testemonial-carousel-->
			</div><!--/.container-->

		</section><!--/.testimonial-->	
		<!-- testemonial End -->
