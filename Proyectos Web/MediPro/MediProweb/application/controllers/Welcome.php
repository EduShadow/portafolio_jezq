<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AMEG extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
	parent::__construct();
	$this->load->model( 'Mpaciente');
	}
	
	 public function index()
	{
		$data['personas1']=$this->Mpaciente->listar_alumnos();
		$this->load->view('alumno/listar_alumno',$data);
	}
	public function agregar()
	{
		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
			
	//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
				
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
			$this->alertas->db($this->Mpaciente->insertar_alumno($data),'AMEG');
		//	}
		//	else{

		//}
		
		} 
		$this->load->view('alumno/agregar_alumno');
	
	}
	/*
	*funcion para editar a la persona
	*@param id
	return void
	*/
	public function editar($id = null)
	{
		$data['persona']=$this->Malumnos->obtener_dato($id);

		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Usuario',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
			
		//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
				$fechaComoEntero= strtotime($this->input->post('Edad',TRUE));
				$edad = date ( "Y") - date( "Y", $fechaComoEntero);
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'Edad'=> $edad,
					'Usuario'=>$this->input->post('Usuario',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
			$this->alertas->db($this->Malumnos->actualizar_alumno($data,$id),'Eduardo');  
	//	}
	//	else{

	//	}
		
		} 
		$this->load->view('alumno/editar_alumno',$data);

	}

	public function eliminar ($id = null){
		$this->alertas->db($this->Malumnos->eliminar_alumno($id),'Eduardo');  
	}
}
 
 