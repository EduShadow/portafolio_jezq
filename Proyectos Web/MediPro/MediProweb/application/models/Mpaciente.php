<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Modelo alumnos
 * Este el modelo para el controlador alumno
 * @author José Eduardo Zamora Quechol
 */

 class Mpaciente extends CI_Model{
/**
 * Listar todos los alumnos
 * @return array
 */



public function listar_alumnos(){
    //select *from persona
    return $this->db->get('paciente')->result();
}


public function listar_doctores(){

	$consulta = $this->db->get("doctor");



	return $consulta->result();
}




/**
 * Listar todos los alumnos
 * @param array
 * return boolean
 */
public function insertar_alumno($data){

    return $this->db->insert('paciente',$data);
}

/**
 * Listar todos los alumnos
 * @param array
 * return boolean
 */
public function insertar_doctor($data){
  
    return $this->db->insert('doctor',$data);
    
}

public function insertar_cita($data){

    return $this->db->insert('cita',$data);
}

public function login ($Correo,$Password){



    $this->db->where('Correo',$Correo);
    $this->db->where('Password',$Password);
    $q= $this->db->get('paciente')->row();
  //  $w = $q::fetch_array();

    if($q!=null){
        $q = (array)$q;
        $this->session->set_userdata($q);
        return true;
    }else{
        return false;
    }

}

public function loginAdm ($Correo,$Password){

    $this->db->where('Correo',$Correo);
    $this->db->where('Password',$Password);
    $q= $this->db->get('paciente')->row();
  //  $w = $q::fetch_array();

    if($q!=null){
        $q = (array)$q;
        $this->session->set_userdata($q);
        return true;
    }else{
        return false;
    }

}

public function logindr ($Correo,$Password){
    $this->db->where('Correo',$Correo);
    $this->db->where('Password',$Password);
    $q= $this->db->get('doctor')->row();
    if($q!=null){
        $q = (array)$q;
        $this->session->set_userdata($q);
        return true;
    }else{
        return false;
    }

}
/**
 * editar alumnos
 * return object
 */
public function obtener_dato($id){
    //echo $id;
    $this->db->where('idPaciente',$id);

    return $this->db->get('paciente')->row();
}

public function obtener_datodr($id){
 //   echo $id;
    $this->db->where('idDoctor',$id);

    return $this->db->get('doctor')->row();
}


/**
 * Actualizar datos del alumno
 * @param array
 * @param int
 * return boolean
 */
public function actualizar_alumno($data,$id){

$this->db->where('idPaciente',$id);
    return $this->db->update('paciente',$data);
}

public function actualizar_doctor($data,$id){

    $this->db->where('idDoctor',$id);
        return $this->db->update('doctor',$data);
    }

/**
 * Eliminar dato alumno
 * @param array
 * @param int
 * return boolean
 */
public function eliminar_paciente($id){
    $this->db->where('idPaciente',$id);
        return $this->db->delete('paciente');
    }

public function eliminar_doctor($id){
	$this->db->where('idDoctor',$id);
		return $this->db->delete('doctor');
	}


function mostrar($valor){
	///HERE

	

	$this->db->like("Especialidad",$valor);
	$this->db->limit(10);
	$consulta = $this->db->get("doctor");
	return $consulta->result();
}

function direccion($valor){

	$query = ' select  D.CostoConsulta, D.Calle, D.Municipio
	from doctor D
   	where D.idDoctor='.$valor.'';
    $resultados = $this->db->query($query);


//	$this->db->like("Nombre",$valor);
//	$consulta = $this->db->get("doctor")->where("Estado from paciente"=="Estado from doctor");


	return $resultados->result();

}

function mostrarr(){

	$query = ' select  D.idDoctor, D.Nombre, D.Apaterno, D.Amaterno, D.Calle, D.Municipio
	from doctor D, paciente P
   	where P.Estado = D.Estado AND P.idPaciente='.$_SESSION['idPaciente'].'';
    $resultados = $this->db->query($query);



//	$this->db->like("Nombre",$valor);
//	$consulta = $this->db->get("doctor")->where("Estado from paciente"=="Estado from doctor");


	//return $consulta->result();

	return $resultados->result();
}


function deleteProfile(){

	$query = 'delete from paciente where paciente.idPaciente ='.$_SESSION['idPaciente'].'';

    $resultados = $this->db->query($query);



//	$this->db->like("Nombre",$valor);
//	$consulta = $this->db->get("doctor")->where("Estado from paciente"=="Estado from doctor");


	//return $consulta->result();

	
}


function mostrarConsulta($valor){

	/*$query = ' select  C.Fecha, C.Hora, C.Sintomas, P.Nombre, P.Apaterno
	from cita C, paciente P, doctor D
   	where C.idDoctor = '.$_SESSION['idDoctor'].' AND C.Fecha='.$valor.'' ;
    $resultados = $this->db->query($query);*/


	$this->db->like("Fecha",$valor);
	$this->db->like("idDoctor",$_SESSION['idDoctor']);
	$consulta = $this->db->get("cita");

	return $consulta->result();
}


function mostrarConsultabyHora($valor){

	/*$query = ' select  C.Fecha, C.Hora, C.Sintomas, P.Nombre, P.Apaterno
	from cita C, paciente P, doctor D
   	where C.idDoctor = '.$_SESSION['idDoctor'].' AND C.Fecha='.$valor.'' ;
    $resultados = $this->db->query($query);*/


	$this->db->like("Hora", $valor);
	$this->db->like("idDoctor",$_SESSION['idDoctor']);
	
	$consulta = $this->db->get("cita");

	return $consulta->result();
}

function mostrarConsultabyNombre($valor){

	/*$query = ' select  C.Fecha, C.Hora, C.Sintomas, P.Nombre, P.Apaterno
	from cita C, paciente P, doctor D
   	where C.idDoctor = '.$_SESSION['idDoctor'].' AND C.Fecha='.$valor.'' ;
    $resultados = $this->db->query($query);*/


	$this->db->like("Nombre", $valor);
	$this->db->like("idDoctor",$_SESSION['idDoctor']);
	
	$consulta = $this->db->get("cita");

	return $consulta->result();
}




	

 }
