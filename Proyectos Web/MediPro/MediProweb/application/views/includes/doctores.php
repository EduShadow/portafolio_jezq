<!-- testemonial Start -->
<section id="res"   class="testemonial">
			<div class="container">

				<p style=" color:black; font: 250% sans-serif; text-align:center;">¿Por qué ir al Doctor?</p><br><br>

				<p style=" color:black;  font: 150% sans-serif; text-align:justify; line-height: 1.5;">
				
				1. Prevenir los riesgos de sufrir una enfermedad antes que ésta aparezca.<br>

				2. Detectar una enfermedad a tiempo para tener la posibilidad de controlarla y eliminarla.<br>

				3. Minimizar el daño ya causado por alguna enfermedad.<br><br>
				
				El coronavirus ha provocado muchos daños colaterales, entre ellos, el aumento de otras patologías. Por ejemplo, un 
				estudio realizado por la Sociedad Española de Cardiología (SEC) indica que las muertes hospitalarias por infarto agudo 
				de miocardio se han duplicado durante la pandemia. El colapso de los hospitales, desbordados ante el continuo goteo de 
				pacientes covid, ha sido, sin duda, uno de los factores responsables de este alarmante aumento de decesos por otras causas.

				Sin embargo, hay que ir más allá. Los expertos coinciden: la falta de prevención de enfermedades, 
				como el cáncer, u otros problemas de salud, como el ictus o el infarto de pacientes que, ante síntomas graves, 
				han preferido no acudir a su centro de salud por miedo al contagio, ha sido otro importante problema a tener en cuenta.</p><br><br>


				<p style=" color:black; font: 250% sans-serif; text-align:center;">¿Qué especialidades tenemos?</p><br><br>
				

				<p style=" color:black; font: 250% sans-serif; text-align:right;">Dentistas</p><br><br>

				<p style=" color:black;  font: 150% sans-serif; text-align:justify; line-height: 1.5;">

			
				<img style="float: left; margin-right: 15px; width: 15%; height:15%;" src="https://i.ibb.co/MnCr6QR/unnamed.png">
				Profesional encargado de la salud bucal que no solo se centra en los dientes, sino también en los diversos órganos que componen la 
				cavidad oral. Los Dentistas son los encargados de prevenir, diagnosticar y tratar enfermedades y trastornos dentales y bucales, 
				realizando procedimientos de rutina y de emergencia a los fines de mejorar la salud de sus pacientes en este particular.
				
				  </p><br><br><br><br>


				  <p style=" color:black; font: 250% sans-serif; text-align:left;">Médicos</p><br><br>

					<p style=" color:black;  font: 150% sans-serif; text-align:justify; line-height: 1.5;">


					<img style="float: right; margin-right: 15px; width: 15%; height:15%;" src="https://i.ibb.co/dL3HWMm/219840730d3ce3fd29bba2dfcb5914a1-car-aacute-cter-m-eacute-dico-mujer-by-vexels.png">
					Los Médicos previenen, diagnostican y tratan diversas enfermedades para mejorar la salud general de sus pacientes. <br>
					<br>
					→ Diagnosticar enfermedades, lesiones y demás trastornos de salud. <br>
					→ Garantizar que los pacientes estén cumpliendo con su tratamiento y de que haya una mejoría en su salud. <br>
					→ Orientar y aconsejar a los pacientes y sus familiares. <br>
			

				</p><br><br><br><br>


				<p style=" color:black; font: 250% sans-serif; text-align:right;">Psicólogos</p><br><br>

				<p style=" color:black;  font: 150% sans-serif; text-align:justify; line-height: 1.5;">

			
				<img style="float: left; margin-right: 15px; width: 15%; height:15%;" src="https://i.ibb.co/PgXdqHZ/psicologo-en-malaga.png">
				Un Psicólogo es un científico de la conducta humana, un experto en el cambio del comportamiento,
				 que enseña recursos psicológicos a las personas para solucionar los problemas de la vida cotidiana, 
				 para así facilitar su adaptación al contexto en el que viven y sentirse bien a corto, medio y largo plazo. <br>

				
				  </p><br><br><br><br>

				  <p style=" color:black; font: 250% sans-serif; text-align:left;">Dermatólogos </p><br><br>

					<p style=" color:black;  font: 150% sans-serif; text-align:justify; line-height: 1.5;">


					<img style="float: right; margin-right: 15px; width: 15%; height:15%;" src="https://i.ibb.co/t4n1zd7/dermatology.png">
					Los dermatólogos son profesionales de la salud que tratan y diagnostican enfermedades de la piel, pelo, uñas y mucosas (aquellas que se encuentran en párpados, boca, nariz, genitales). 
					Estas enfermedades se pueden presentar en pacientes de todas las edades, desde recién nacidos hasta ancianos, abarcando cada una de las etapas de la vida. 


					</p><br><br><br><br>

				

				<div class="gallary-header text-center">
				<br><br>
					<h2>
						Doctores
					</h2>
					<p>
						Doctores Registrados  
					</p>
					<br><br>
					

				
		
					</div><!--/.gallery-header-->
		
			

			
						<div id="lista" >

		
			

			</div><!--/.container-->

		</section><!--/.testimonial-->	
		<!-- testemonial End -->



		<script type="text/javascript">
		


		//$(document).on("ready", inicio);
		
		$(document).ready(function() {
			inicio();
		});
		
		function inicio() {
			mostrarDatos("");
		
			$("#buscar").keyup(function () {
				buscar = $("#buscar").val();
				mostrarDatos(buscar);
			});
			$("form").submit(function (event) {
				event.preventDefault();
				$.ajax({
					url: $("form").attr("action"),
					type: $("form").attr("method"),
					data: $("form").serialize(),
					success: function (respuesta) {
						alert(respuesta);
					},
				});
			});
		}
		
		function mostrarDatos(valor) {
			$.ajax({
				url: "AMEG/mostrar",
				type: "POST",
				data: { buscar: valor },
				success: function (respuesta) {
				//	alert(respuesta);
					var registros = eval(respuesta);
		
			html = "<table class='table table-responsive table-bordered' align='center' ><thead>";
			html += "<tr bgcolor='#87CEEB'><th >ID</th><th>Nombres</th><th>Apellido</th><th>Especialidad";
			html += "</thead><tbody>";
			for (var i=0; i < registros.length; i++) {
				html +=
				"<tr><td >"+registros[i]["idDoctor"]+"</td><td>"+registros[i]["Nombre"]+"</td><td>"+registros[i]["Apaterno"]+"</td><td>"+registros[i]["Especialidad"]+"</td></tr>" ;
					
			};
			html += "</tbody></table>";

			$("#lista").html(html);
				},
			});
		}
		
		
		</script>
