<!-- testemonial Start -->
<section id="res"   class="testemonial">
			<div class="container">


	
				<div  class="gallary-header text-center">
					<h2>
				Mis Consultas
		
					</h2>
					<p>
				Consultas de 	<?=$personaDoctor->Nombre?>				</p>
			<!--	<p id="<?=$personaDoctor->idDoctor?>"><?=$personaDoctor->Nombre?></p>-->
			<br>
				

				</div><!--/.gallery-header-->


				<center>
				<form method="post" id="miForm" >
				
				Fecha: <input placeholder="Ingrese aquí" type="date" name="buscar" id="buscar">
			 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				Hora: <input  type="time" name="buscarHora" id="buscarHora">

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				Nombre: <input placeholder="Ingrese aquí" type="text" name="buscarNombre" id="buscarNombre"> &nbsp;&nbsp;&nbsp;&nbsp;
			
				<a class="boton_personalizado" id="botonPersonalizado" name="botonPersonalizado" type="submit" >Limpiar filtrado</a>
				<br><br>
				<div id="lista" class="col-lg-12">

				</div>
				</form>
				
				</center>

			
			</div><!--/.container-->

		</section><!--/.testimonial-->	
		<!-- testemonial End -->


<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
		<script type="text/javascript">
		
					var pusher = new Pusher('766a096d82d621587061', {
						cluster: 'mt1'
					});

					/*var channel = pusher.subscribe('misNotificaciones');
					channel.bind=('recibirNotificacion', function(data) {
						$('contenedorNotificaciones').append('<div>'+data.message +'</div>');
					});*/

			
					var channel = pusher.subscribe('misNotificaciones');
						channel.bind('recibirNotificacion', function(data) {

							;
						//	A=6;
						//	console.log(<?=$personaDoctor->idDoctor?>);
							A=<?=$personaDoctor->idDoctor?>;
							B=parseInt(data.idDoctor);

						//	if(parseInt(JSON.stringify(data.idDoctor)) == JSON.stringify(data.idElDr)){ 
							if(A == B){ 
							Push.create("¡Enhorabuena!",{
							body: "Tiene una nueva cita con: "+ JSON.stringify(data.message),
							icon: "https://i.ibb.co/R2vK2Gj/calendario.png",
							timeout: "10000",
							onClick: function () {
								this.close();
							}
								});
							}

					//	alert(JSON.stringify(data));
					});
		</script>

<script type="text/javascript">
		
	
		
		$(document).ready(function() {
			inicio();
		});
		
		function inicio() {
			mostrarDatos("");
		
			$("#buscar").change(function () {
				buscar = $("#buscar").val();
				mostrarDatos(buscar);
			});
			$("#buscarHora").change(function () {
				buscar = $("#buscarHora").val();
				mostrarDatosbyHora(buscar);
			});

			$("#buscarNombre").keyup(function () {
				buscar = $("#buscarNombre").val();
				mostrarDatosbyNombre(buscar);
			});

			$("#botonPersonalizado").click(function(event) {

				limpiarDatos();

			});
			mostrarDatosbyHora
			$("form").submit(function (event) {
				event.preventDefault();
				$.ajax({
					url: $("form").attr("action"),
					type: $("form").attr("method"),
					data: $("form").serialize(),
					success: function (respuesta) {
						alert(respuesta);
					},
				});
			});
		}

		function limpiarDatos(){
	
			document.getElementById("miForm").reset();
			mostrarDatos("");
		}
		
		function mostrarDatos(valor) {
			$.ajax({
				url: "mostrarConsulta",
				type: "POST",
				data: { buscar: valor },
				success: function (respuesta) {
				//	alert(respuesta);
					var registros = eval(respuesta);
				
					html = "<table class='table table-responsive table-bordered' align='center' ><thead>";
					html += "<tr bgcolor='#87CEEB'><th >ID</th><th>Fecha</th><th>Hora</th><th>Sintomas</th><th>Nombre del paciente";
					html += "</thead><tbody>";
					for (var i=0; i < registros.length; i++) {
						html +=
						"<tr><td >"+registros[i]["idCita"]+"</td><td>"+registros[i]["Fecha"]+"</td><td>"+registros[i]["Hora"]+"</td><td>"+registros[i]["Sintomas"]+"</td><td>"+registros[i]["Nombre"]+"</td></tr>" ;
							
					};
					html += "</tbody></table>";
		
					$("#lista").html(html);
				},
			});
		}
		
		function mostrarDatosbyHora(valor) {
			$.ajax({
				url: "mostrarConsultabyHora",
				type: "POST",
				data: { buscar: valor },
				success: function (respuesta) {
				//	alert(respuesta);
					var registros = eval(respuesta);
			
				
					html = "<table class='table table-responsive table-bordered' align='center' ><thead>";
					html += "<tr bgcolor='#87CEEB'><th >ID</th><th>Fecha</th><th>Hora</th><th>Sintomas</th><th>Nombre del paciente";
					html += "</thead><tbody>";
					for (var i=0; i < registros.length; i++) {
						html +=
						"<tr><td >"+registros[i]["idCita"]+"</td><td>"+registros[i]["Fecha"]+"</td><td>"+registros[i]["Hora"]+"</td><td>"+registros[i]["Sintomas"]+"</td><td>"+registros[i]["Nombre"]+"</td></tr>" ;
							
					};
					html += "</tbody></table>";
		
					$("#lista").html(html);
				},
			});
		}

		function mostrarDatosbyNombre(valor) {
			$.ajax({
				url: "mostrarConsultabyNombre",
				type: "POST",
				data: { buscar: valor },
				success: function (respuesta) {
				//	alert(respuesta);
					var registros = eval(respuesta);
			
				
					html = "<table class='table table-responsive table-bordered' align='center' ><thead>";
					html += "<tr bgcolor='#87CEEB'><th >ID</th><th>Fecha</th><th>Hora</th><th>Sintomas</th><th>Nombre del paciente";
					html += "</thead><tbody>";
					for (var i=0; i < registros.length; i++) {
						html +=
						"<tr><td >"+registros[i]["idCita"]+"</td><td>"+registros[i]["Fecha"]+"</td><td>"+registros[i]["Hora"]+"</td><td>"+registros[i]["Sintomas"]+"</td><td>"+registros[i]["Nombre"]+"</td></tr>" ;
							
					};
					html += "</tbody></table>";
		
					$("#lista").html(html);
				},
			});
		}
		
		</script>
		